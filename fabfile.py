import os
import functools
from datetime import datetime
from secrets import token_urlsafe
from shlex import quote

from fabric import Connection, task
from invocations import console
from patchwork.files import exists
from paramiko.ssh_exception import PasswordRequiredException
import ruamel.yaml
from ruamel.yaml.compat import StringIO
from pathlib import Path

yaml = ruamel.yaml.YAML()

LINUX_PACKAGES = [
    'python3.7',
    'python3.7-dev',
    'libpython3.7-dev',
    'python3-pip',
    # 'pipenv',
    'apache2',
    'mariadb-server',
    'python3-mysqldb',
    'default-libmysqlclient-dev',
    'libapache2-mod-wsgi-py3',
    'virtualenv',
    'nodejs',
    # 'npm',
]

PROJECT_NAME = 'ohai'
PROJECT_PATH = os.path.join(os.sep, 'srv', PROJECT_NAME)
CURRENT_RELEASE = os.path.join(PROJECT_PATH, 'releases', 'current')
REPO = 'gitlab.com/lulzbot3d/ohai-kit.git'
APACHE_CONFIG = 'linux_configs/ohai.apache.conf'

MIN_DISK_SPACE = 250_000 #kb

BASE_ENV = {'PIPENV_VENV_IN_PROJECT': 'True'}


def get_connection(ctx):
    try:
        with Connection(
                host=ctx.host,
                port=ctx.port,
                user=ctx.user,
                connect_kwargs=ctx.connect_kwargs,
                inline_ssh_env=True
        ) as c:
            return c
    except PasswordRequiredException:
        print("Your ssh key requires a passphrase. Use fab --prompt-for-passphrase <command>")
    except Exception as e:
        print("Could not get a connection", e)
        return None

def generate_deploy_tag():
    return datetime.now().strftime('%Y%m%dT%H%M')

def get_release_list(c):
    response = c.run('ls -r %s/releases/ | grep -P "^\d{8}T\d{4}"' % PROJECT_PATH, hide=True)
    return response.stdout.splitlines()

@task
def staging(ctx):
    ctx.server = 'staging'
    ctx.branch = 'staging'
    ctx.host = "ohai-staging.lulzbot.com"
    ctx.port = 61

@task
def old(ctx):
    ctx.server = 'production'
    ctx.branch = 'master'
    ctx.host = "ohai.lulzbot.com"
    ctx.port = 61
    # ctx.user = 'ohai'

@task
def production(ctx):
    ctx.server = 'production'
    ctx.branch = 'master'
    ctx.host = "ec2-54-153-125-43.us-west-1.compute.amazonaws.com"
    ctx.port = 22
    # ctx.user = 'ohai'

@task()
def setup(ctx):
    with get_connection(ctx) as c:
        c.sudo('apt-get -y install software-properties-common')
        c.sudo('apt update')
        c.sudo('apt-get -y install %s' % " ".join(LINUX_PACKAGES))
        c.sudo('pip3 install pipenv')
        c.put(APACHE_CONFIG)
        c.sudo('mv ohai.apache.conf /etc/apache2/conf-available/ohai.conf')
        c.sudo('a2enmod ssl')

        c.sudo('mkdir -p %s/shared/ohai' % PROJECT_PATH)
        c.sudo('mkdir -p %s/releases' % PROJECT_PATH)

        settings = load_settings(c, ctx.server)

        c.sudo('mysql -u root -e "CREATE DATABASE %s;"' % settings['MYSQL_DATABASE_NAME'], warn=True)
        c.sudo('mysql -u root -e "GRANT ALL PRIVILEGES ON %s.* TO \'%s\'@\'localhost\' IDENTIFIED BY \'%s\';"' % (
            settings['MYSQL_DATABASE_NAME'], settings['MYSQL_USERNAME'], settings['MYSQL_PASSWORD']
        ))
        c.sudo('echo -e %s | sudo tee %s/shared/ohai/settings.yml' % (
            quote(dump_settings(settings)), PROJECT_PATH
        ))

        if console.confirm("Would you like to edit settings?"):
            c.sudo('vi %s/shared/ohai/settings.yml' % PROJECT_PATH)
        c.sudo('chown -R ohai:www-data %s' % PROJECT_PATH)
        print('---\nCompleted setup')

@task()
def settings(ctx):
    with get_connection(ctx) as c:
        c.sudo('vi %s/shared/ohai/settings.yml' % PROJECT_PATH)

@task()
def deploy(ctx, branch=None):
    branch = branch or ctx.branch
    with get_connection(ctx) as c:
        check_disk_space(c)
        deploy_path = create_deploy_dir(c, generate_deploy_tag())
        copy_project_files(c, deploy_path, branch)
        build_project(c, deploy_path)
        run_migrations(c, deploy_path)
        go_live(c, deploy_path)
        print('Released branch %s at %s' % (branch, deploy_path))

@task()
def restart(ctx):
    with get_connection(ctx) as c:
        c.sudo('service apache2 reload')

@task()
def rebuild_search_index(ctx):
    with get_connection(ctx) as c:
        c.run('cd %s && pipenv run ./manage.py rebuild_index' % CURRENT_RELEASE, env=BASE_ENV)

@task()
def list_deploys(ctx):
    with get_connection(ctx) as c:
        print('\n'.join(get_release_list(c)))

@task()
def rollback(ctx, to=None):
    with get_connection(ctx) as c:
        if not to:
            to = 1
        try:
            deploy_tag = get_release_list(c)[int(to)]
        except:
            deploy_tag = to

        deploy_path = os.path.join(PROJECT_PATH, 'releases', deploy_tag)
        if not exists(c, deploy_path):
            raise Exception("Did not rollback. No release found matching %s" % deploy_path)

        new_deploy_path = os.path.join(PROJECT_PATH, 'releases', generate_deploy_tag() + 'R')
        c.run('ln -s %s %s' % (deploy_path, new_deploy_path))

        go_live(c, new_deploy_path)
        print('Rolled back to %s' % deploy_tag)

@task()
def prune(ctx, keep=10):
    with get_connection(ctx) as c:
        available_releases = get_release_list(c)
        if len(available_releases) > keep:
            c.run('cd %s/releases && rm -rf %s' % (PROJECT_PATH, " ".join(available_releases[10:])))
            print('Pruned %s releases. Kept previous %s.' % (len(available_releases) - keep, keep))
        else:
            print('No releases pruned. Kept previous %s.' % keep)

@task()
def disk_space(ctx):
    with get_connection(ctx) as c:
        c.run('df -lh %s' % PROJECT_PATH)

@task()
def error_log(ctx, n=15):
    with get_connection(ctx) as c:
        c.sudo('tail -n %s /var/log/apache2/error.log' % int(n))


def load_settings(c, server):
    try:
        settings = c.run('cat %s/shared/ohai/settings.yml' % PROJECT_PATH, hide=True)
        settings = yaml.load(settings.stdout)
        print('using existing settings')
    except:
        try:
            settings = yaml.load(Path('ohai/settings.%s.yml' % server))
            print('using local %s settings' % server)
        except FileNotFoundError:
            settings = yaml.load(Path('ohai/settings.example.yml'))
            print('using local example settings')

    settings['SECRET_KEY'] = settings.get('SECRET_KEY', token_urlsafe(60))
    settings['MYSQL_DATABASE_NAME'] = settings.get('MYSQL_DATABASE_NAME', 'ohaikit')
    settings['MYSQL_USERNAME'] = settings.get('MYSQL_USERNAME', 'ohai')
    settings['MYSQL_PASSWORD'] = settings.get('MYSQL_PASSWORD', token_urlsafe(28))
    return settings

def dump_settings(settings):
    settings_stream = StringIO()
    yaml.dump(settings, settings_stream)
    return settings_stream.getvalue()

def check_disk_space(c):
    check_disk = c.run('df -l %s --output=avail | tail -1' % PROJECT_PATH, hide=True)
    available_space = int(check_disk.stdout)
    if (available_space < MIN_DISK_SPACE):
        raise Exception("insufficent disk space for {}. {}kb remaining".format(PROJECT_PATH, available_space))

def create_deploy_dir(c, deploy_tag):
    deploy_path = os.path.join(PROJECT_PATH, 'releases', deploy_tag)
    if exists(c, deploy_path):
        raise Exception('deploy directory already exists: %s' % deploy_path)
    create_directory = c.run('mkdir -p %s' % deploy_path)
    if create_directory.ok:
        return deploy_path
    else:
        raise Exception('Failed to create deploy directory')

def copy_project_files(c, deploy_path, branch):
    print('copying files')
    c.run('git clone https://%s %s --branch %s --single-branch' % (REPO, deploy_path, branch), hide=True)
    c.run('rm -rf %s/.git*' % deploy_path)
    c.run('cp -r %s/shared/ohai %s' % (PROJECT_PATH, deploy_path))

def build_project(c, deploy_path):
    print('building project')
    c.run('cd %s && pipenv install' % deploy_path, env=BASE_ENV, hide=True)
    javascript_path = deploy_path + '/ohai_kit/static'
    if exists(c, javascript_path + '/package.json'):
        c.run('cd %s && npm install' % javascript_path, env=BASE_ENV, hide=True)

    c.run('echo -e "from ohai.settings import *\nSTATIC_ROOT = \'%s/staticfiles\'" >> %s/build_settings.py' % (deploy_path, deploy_path), env=BASE_ENV, hide=True)
    if 'compilescss' in c.run('cd %s && pipenv run ./manage.py help' % deploy_path, env=BASE_ENV, hide=True).stdout:
        c.run('cd %s && pipenv run ./manage.py compilescss --settings=build_settings' % deploy_path, env=BASE_ENV, hide=True)
    c.run('cd %s && pipenv run ./manage.py collectstatic --ignore=*.scss --settings=build_settings' % deploy_path, env=BASE_ENV, hide=True)
    c.run('rm %s/build_settings.py' % deploy_path)

    c.run('touch %s/staticfiles/robots.txt' % deploy_path)
    c.run('touch %s/staticfiles/favicon.ico' % deploy_path)

def run_migrations(c, deploy_path):
    print('running migrations')
    c.run('cd %s && pipenv run ./manage.py migrate' % deploy_path, env=BASE_ENV)

def go_live(c, deploy_path):
    print('deploying')
    c.run('ln -sfn %s %s' % (deploy_path, CURRENT_RELEASE))
    c.sudo('a2enconf ohai')
    c.sudo('service apache2 reload')
