Open Hardware Assembly Instruction Kit
---

OHAI-Kit is a web application using the Django web framework.
It's a comprehensive, accessible source for putting together your newest piece of machinery. Aiming to create clear and concise instructions for optimal assembly, OHAI-Kit features step-by-step instructions, illustrative pictures, and thorough checkpoints to be sure all steps have been covered.

## Technology and Stack
Familiarize yourself with the following technologies before working on this project.

- [Python 3.7](https://www.python.org/downloads/release/python-372/)
- [Pipenv](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)
- [MariaDB](https://mariadb.org)
- [Django 2.1](https://docs.djangoproject.com/en/2.1/)

## Setup
1. `pipenv install` - Create virtual environment and install dependencies*.
1. `pipenv shell` - Use virtual environment (`exit` to stop).
1. `cp ohai/settings.example.yml ohai/settings.yml` - Edit `settings.yml` as necessary.
1. Create MariaDB database. See [Database Setup](#database-setup) if necessary.
1. `python manage.py migrate` - Run database migrations
1. `python manage.py loaddata seeds` - Load seeds file.
1. `python manage.py createsuperuser` - Create an admin account for yourself.
1. `cd ohai_kit/static && npm install && cd -` - Install sass and javascript dependencies

Run the development server and test suite to verify successful deployment.

## Development server
- You must be in the correct [virtual environment](https://realpython.com/python-virtual-environments-a-primer/) (`pipenv shell`)
- `python manage.py runserver`
- View site at `http://localhost:8000/admin`
- Use the admin account you created with `python manage.py createsuperuser`.

## Testing
- `python manage.py test`

## Configuration

Once installed you can configure ohai-kit using the included admin interface.
See the [configuration document](./docs/configuration.md).

## Deployment
- Put any secrets and passwords in your environment. Referrence the keys in `ohai/settings.yml.example`. You may want to use `.bash_profile` for the webserver user account with appropriately restricted permissions.
  - Generate a `SECRET_KEY` with `pipenv run python manage.py shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())'`.

## Troubleshooting/OS variances

- If you're having a hard time getting MySQLClient to work, try running `pipenv uninstall mysqlclient` and then `LDFLAGS=-L/usr/local/opt/openssl/lib pipenv install mysqlclient`. It appears to have an issue with installing the package with the wrong SSL library, which this command solves as of this writing.

### Database Setup

1. `mysql -u root`
1. `CREATE DATABASE ohaidb;`
1. `exit;`
1. Configure `ohai/settings.yml` as necessary.

## Development Process

- See [PROCESS](./PROCESS.md)
- Follow [PEP 8](https://www.python.org/dev/peps/pep-0008/) with the optional 100 character line length.
