#!/usr/bin/env python
import os
from setuptools import find_packages, setup
from setuptools.command.test import test as TestCommand

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to py.test")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = []

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)


setup(
    name='ohai_kit',
    version='2.0',
    packages=find_packages(exclude=['tests*']),
    include_package_data=True,
    install_requires=[
        'django>=2.0',
        'easy_thumbnails',
        'django-markdown-app',
    ],
    tests_require=[
        'factory-boy',
        'pytest-django',
        'django_webtest',
        'pytest-pep8',
        'pytest-pylint',
    ],
    cmdclass={'test': PyTest},
    license='GNU Affero General Public License v3',
    description='Open Hardware Assembly Instruction Kit',
    long_description=README,
    url='https://code.alephobjects.com/diffusion/OK/',
    author='Avea Palecek',
    author_email='',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 2.2',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
