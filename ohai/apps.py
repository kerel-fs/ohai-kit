from django.contrib.admin.apps import AdminConfig


class OHAIAdminConfig(AdminConfig):
    default_site = 'ohai.admin.OHAIAdminSite'
    