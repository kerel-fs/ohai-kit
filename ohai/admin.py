from django.contrib import admin


class OHAIAdminSite(admin.AdminSite):
    site_header = 'Open Hardware Assembly Instructions Admin'
