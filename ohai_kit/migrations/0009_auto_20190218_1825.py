# Generated by Django 2.1.7 on 2019-02-18 18:25

import adminsortable.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ohai_kit', '0008_auto_20190131_1709'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectset',
            name='parent_set',
            field=adminsortable.fields.SortableForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ohai_kit.ProjectSet'),
        ),
        migrations.AddField(
            model_name='projectset',
            name='parent_set_order',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
        ),
    ]
