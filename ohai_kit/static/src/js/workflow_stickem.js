(function() {
  var positionStickySupport = function() {var el=document.createElement('a'),mStyle=el.style;mStyle.cssText="position:sticky;position:-webkit-sticky;position:-ms-sticky;";return mStyle.position.indexOf('sticky')!==-1;}();

  $(document).ready(function() {
    if (positionStickySupport) return;

    var $content = $('#content_area');

    $content.imagesLoaded()
      .done( function( instance ) {

        // Run stickem after images loaded, to get accurate heights.
        $content.stickem({
          item: '.text_column',
          container: '.work_step'
        });
      });
  });
})();
