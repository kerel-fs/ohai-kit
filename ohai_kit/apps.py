from django.apps import AppConfig


class OHAIKitConfig(AppConfig):
    name = 'ohai_kit'
    verbose_name = 'OHAI Kit'
