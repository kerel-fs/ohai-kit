from django.urls import path
from django.contrib.auth.views import LogoutView

from . import views
from .models import OhaiKitSetting

urlpatterns = [
    path(r'', views.system_index, name='index'),
    path(r'group/', views.ungrouped_view, name='misc_group'),
    path(r'group/<slug:group_slug>/', views.group_view, name='named_group'),
    path(r'workflow/<slug:project_slug>/', views.project_view, name='project'),
    path(r'workflow/<slug:project_slug>/<slug:parent_group_slug>/', views.project_view, name='project_with_parent'),
    path(r'workflow/<slug:project_slug>/start/',
        views.start_job, name='start_job'),

    path(r'jobs/<int:job_id>/', views.job_status, name='job_status'),
    path(r'jobs/<int:job_id>/close', views.close_job, name='close_job'),
    path(r'jobs/<int:job_id>/update', views.update_job, name='update_job'),

    path(r'session_settings/', views.session_settings, name='session_settings'),

    path(r'project/<slug:project_slug>/', views.guest_workflow, name='guest_workflow'),
    path(r'project/<slug:project_slug>/<slug:parent_group_slug>/', views.guest_workflow, name='guest_workflow_with_parent'),

    path(r'search/', views.ProjectSearchView.as_view(), name='project_search'),

    path(r'accounts/guest_bypass/', views.guest_access, name='guest_access'),
    path(r'accounts/login/', views.worker_access,
        {'template_name': 'ohai_kit/login.html',
         'extra_context': {'touch_emulation': False, 'settings': OhaiKitSetting.load()}
        }, name='login'
    ),
    path(r'accounts/logout/', LogoutView, {'next_page': '/'}),
]
