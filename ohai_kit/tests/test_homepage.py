from django_webtest import WebTest
from ohai_kit.models import OhaiKitSetting


class HomepageTestCase(WebTest):
    def setUp(self):
        OhaiKitSetting.set(guest_mode=True)

    def test_homepage_shows_title(self):
        response = self.app.get('/')
        self.assertContains(response, 'Open Hardware Assembly Instructions')
