# Working on Open Hardware Assembly Instructions Kit

## Developer:

1. Identify the issue you will be working on.

2. Clone the repo.

3. Create a branch locally named after the feature/issue that you are working on. Include the ticket number in the branch name.

  - Example: `T805-add-search`

4. Create a feature test for your feature.

5. Implement your feature and satisfy relevant test(s).

6. Repeat 4 and 5 as needed.

  - WIP-commit as needed, too -- preferably after a each iteration. See Developer Notes below.

7. Run the entire test suite.

  - Make sure your changes don't break anything else.  Fix anything broken.

8. When you reach the point where the issue in the ticket has been addressed, notify the maintainer that the branch is ready to be reviewed and merged into staging.

## Maintainer:

1. Review the code in the branch using a difftool.
2. If the changes look like they could address feature/issue, merge the branch into staging.
3. If there are issues with the patch, provide comments on the ticket associated with the feature/issue indicating what the issues are, and what might be done about them if possible.

## Reviewer:

1. Navigate to the staging server url.
2. Confirm that the feature works as expected, or that the issue is solved.
3. Notify the team of any issues by making a comment on the ticket associated with the feature/issue, or confirm that the feature/issue is resolved by commenting in the associated ticket.

## Developer Notes:

1. Never commit directly to master.
2. Commit messages should start with a capital letter, and do not need a period at the end. 
3. In the commit message, convey information about why the commit was made not what the commit does.
