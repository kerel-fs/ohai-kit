## Fabric documentation

Fabric is a Python library that runs shell commands over SSH in order to automate routine server management tasks.

### Getting started

Fabric scripts for this project are defined in the `fabfile.py` in the project root directory. You will need Fabric 2 installed to run the commands. While in the project, run `pipenv install -d` to install the dev-packages if you haven't already. Then run `pipenv shell` to activate the environment.

Fabric commands follow this pattern
```
$ fab <key-options> <commands> <command-options>
```

A common key option is `--prompt-for-passphrase` which will ask to unlock your ssh key if it requires a passphrase (recommended).

In some cases, commands can be chained. This is useful for setting the server environment you wish to target (`staging` or `production`)

### Quick Commands

#### Example: Deploy Specified Branch to Staging 
```
$ fab --prompt-for-passphrase staging deploy --branch master
```

#### Example: Restart Production
```
$ fab --prompt-for-passphrase production restart
```

#### Example: Rollback Production to Previous Deploy
```
$ fab --prompt-for-passphrase production rollback
```

### Command List

#### staging

- Sets host to staging
- Sets default deploy branch to staging

#### production

- Sets host to production
- Sets default deploy branch to master

#### setup

- Installs required linux packages
- Uploads apache conf from `linux_configs`
- Creates required application directories
- Creates Mariadb database if none exists
- Creates database user if necessary and adds generated password to settings.
- corrects permissions

#### settings

- Opens the settings file for editing
- Changes will not take effect until the next deploy

#### deploy (--branch <branch-name>)

- Deploys the default branch or specified branch
- Creates a new directory with the current datetime
- Symlinks to `current` when complete and restarts

#### restart

- Restarts apache

#### rebuild-search-index

- Rebuilds the haystack search index

#### list-deploys

- Lists the deploys that exist on the server (useful if you need to roll back to a specific point)

#### rollback (--to)

- If no options provided will roll back to the previous deploy
- Creates a symlink with the current datetime + `R` to provide a record of the rollback
- If a number is provided as an option, will go back the specified number of releases
- If a deploy tag is provied, will go back to that release (see `list_deploys`)

#### prune (--keep=10):

- Deletes old deploys to free up disk space
- If keep option is not provided, keep the newest 10 releases and deletes the older.

#### disk-space

- Shows the disk usage on the disk containing the project

#### error-log (-n=15)

- Shows last n lines of the Apache error log
